let sentence = ["My","Favorite","Fast Food","is","Burger King"]

let fastFood = sentence.join(" ");
console.log(fastFood);

let roles = ["Paladin","Priest","Engineer","Assassin","Thief"];

//for testing on empty arr, uncomment line 9 and comment out line 6
// let roles = []

function removeLastItem() {
    if (!roles.length) {
        alert ("No Roles Available");
    } else {
        const removedItem = roles.splice(-1);
        alert (removedItem + " has been deleted!")
    }
}


function addItem(str) { 
    const sanitizedStr = str.charAt(0).toUpperCase();
    const exists = roles.includes(sanitizedStr);

    if (exists) {
        alert ("Role already added.");
    } else {
        roles.push (sanitizedStr);
        alert("Role added. There are " + roles.length + " roles available.");
    }
}

removeLastItem();

addItem("Spy");
